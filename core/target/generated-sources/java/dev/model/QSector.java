package dev.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSector is a Querydsl query type for Sector
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSector extends EntityPathBase<Sector> {

    private static final long serialVersionUID = 831287076L;

    public static final QSector sector = new QSector("sector");

    public final StringPath description = createString("description");

    public final ListPath<Employee, QEmployee> employeeList = this.<Employee, QEmployee>createList("employeeList", Employee.class, QEmployee.class, PathInits.DIRECT2);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public QSector(String variable) {
        super(Sector.class, forVariable(variable));
    }

    public QSector(Path<? extends Sector> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSector(PathMetadata metadata) {
        super(Sector.class, metadata);
    }

}

