# Spring Boot Skeleton #

## Objectives ##

Provide a modularized spring boot skeleton.

## Setting Up Environment ##

### Development ###

#### Download and install docker-ce ####

* Linux: https://www.digitalocean.com/community/tutorials/como-instalar-e-usar-o-docker-no-ubuntu-16-04-pt

#### Download and install docker-compose ####

https://docs.docker.com/compose/install/#install-compose

#### Setting Up Database Connection ####

Create the file `src/main/resources/application.properties`, with the following text:

```
## Spring DATASOURCE (DataSourceAutoConfiguration & DataSourceProperties)
spring.datasource.url = jdbc:mysql://localhost:3306/RADIO_CORE?useSSL=false
spring.datasource.username = root
spring.datasource.password = root


## Hibernate Properties
# The SQL dialect makes Hibernate generate better SQL for the chosen database
spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.MySQL5InnoDBDialect

# Hibernate ddl auto (create, create-drop, validate, update)
spring.jpa.hibernate.ddl-auto = update

# Development Logging
logging.level.org.hibernate.SQL=DEBUG
logging.level.org.hibernate.type=TRACE

# Spring Server Properties
server.servlet.context-path=/api
```

You may change username, password and url for the connection to fit your environment.

#### Composing up! ####

In your application directory, run:

```bash
docker-compose up -d
```

Unfortunatelly, you'll need to run this command every time you make a change, so Java can compile and docker can recreate Tomcat's service :(

So, don't forget to compose down, compose up every time you make changes!

#### Composing down :( ####

To shut down the application, enter your application directory and run:

```bash
docker-compose down
```
