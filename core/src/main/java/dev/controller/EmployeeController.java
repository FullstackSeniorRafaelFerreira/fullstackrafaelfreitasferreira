package dev.controller;

import com.querydsl.core.types.Predicate;
import dev.model.Employee;
import dev.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RestController
@RequestMapping(value = "/employee", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping
    public ResponseEntity<?> getAllEmployee() {
        return ResponseEntity.ok(employeeService.getAllEmployee());
    }

    @GetMapping("search")
    public ResponseEntity<?> findEmployee(
            @QuerydslPredicate(root = Employee.class) Predicate employeePredicate,
            @RequestParam(required = false) String q
    ) {
        if (q != null && employeePredicate == null) {

            return  ResponseEntity.ok(employeeService.findLikeEmployee(q));

        } else if (q != null){

            return  ResponseEntity.ok(employeeService.findEmployee(employeePredicate));

        } else {
            return  ResponseEntity.ok(employeeService.findEmployee(employeePredicate));
        }

    }

    @PostMapping
    public ResponseEntity<?> createEmployee(@RequestBody Employee employee) {
        employeeService.saveEmployee(employee);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteEmployee(@PathVariable Long id) {
        employeeService.deleteEmployee(id);
        return ResponseEntity.noContent().build();
    }


}
