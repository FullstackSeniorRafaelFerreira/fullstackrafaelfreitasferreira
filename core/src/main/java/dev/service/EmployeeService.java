package dev.service;

import com.querydsl.core.types.Predicate;
import dev.model.Employee;
import dev.model.QEmployee;
import dev.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {


    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private AgeLimit ageLimit;

    public List<Employee> getAllEmployee () {
       return this.employeeRepository.findAll();
    }

    public List<Employee> findEmployee(Predicate employeePredicate) {
        return (List<Employee>) employeeRepository.findAll(employeePredicate);
    }

    public List<Employee> findLikeEmployee(String searchTerm) {
        searchTerm = "%"+searchTerm+"%";
        Predicate employeePredicate = QEmployee.employee.name.likeIgnoreCase(searchTerm)
                .or(QEmployee.employee.cpf.likeIgnoreCase(searchTerm))
                .or(QEmployee.employee.email.likeIgnoreCase(searchTerm));

        return (List<Employee>) employeeRepository.findAll(employeePredicate);
    }

    public void saveEmployee(Employee employee) {
        ageLimit.lessThan(employee);
        employeeRepository.save(employee);
    }

    public void deleteEmployee(Long id) {
        employeeRepository.deleteById(id);
    }

}
