package dev.service;

import dev.model.Employee;
import dev.model.Sector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AgeLimit {

    @Autowired
    private SectorService sectorService;

    public Boolean lessThan(Employee employee) {
        Optional<Sector> sector = sectorService.getSectorByEmployee(employee);

        
        DateFormat dateFormat = DateFormat.getDateInstance();
        Date date = dateFormat.parse();
        return (new Date().getTime() - date.getTime())

        return true;
    }

    public Boolean greaterThan() { return true; }

}
