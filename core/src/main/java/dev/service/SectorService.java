package dev.service;

import com.querydsl.core.types.Predicate;
import dev.model.Employee;
import dev.model.QSector;
import dev.model.Sector;
import dev.repository.SectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SectorService {

    @Autowired
    private SectorRepository sectorRepository;

    public List<Sector> getAllSector() {
        return sectorRepository.findAll();
    }

    public Optional<Sector> getSectorByEmployee(Employee employee) {
        Predicate sectorByEmployeePredicate = QSector.sector.employeeList.contains(employee);

        return sectorRepository.findOne(sectorByEmployeePredicate);
    }
}
